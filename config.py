from pathlib import Path

input_file_location = Path("./data")
input_file = input_file_location / "poisson"


save_fig = True
output_file_location = Path("./output")
output_file = output_file_location / "poisson.png"

read_line_number = True
line_number_read_print = 10000
data_type = "int"

# plot types
## "histogram", 
plot_type = "histogram"



histogram_density_function = True

plot_title = "Histogram of poisson"

skip_bins = True
histogram_bins = 100