import config
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np

lineNumber = 0
with open(config.input_file, "r") as file:
    data = []
    for line in file:
        lineNumber += 1
        if config.data_type == "int":
            data.append(int(line))
        elif config.data_type == "float":
            data.append(float(line))
        if config.read_line_number:
            if lineNumber % config.line_number_read_print == 0:
                print(f"Processed line {lineNumber}, which is {line}")



if config.plot_type == "histogram":
    if config.skip_bins:
        sns.histplot(data, discrete = True)
    else:
        sns.histplot(data, bins=config.histogram_bins, kde = config.histogram_density_function)
    
plt.gca().set(title=config.plot_title)

if config.save_fig:
    plt.savefig(config.output_file)
else:
    plt.show()